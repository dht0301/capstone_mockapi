const BASE_URL = "https://633ec05c0dbc3309f3bc546d.mockapi.io";

// R (read)
function loadProductFromAPI() {
  return axios
    .get(`${BASE_URL}/Sale`)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}
