// CURD
// C: create -> có tác động vào data
// U: Update  -> có tác động vào data
// R: read
// D: Delete  -> có tác động vào data
// tạo biến BASE_URL
const BASE_URL = "https://633ec05c0dbc3309f3bc546d.mockapi.io";

// R (read)
function loadProductFromAPI() {
  return axios
    .get(`${BASE_URL}/Sale`)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}

function saveProductToAPI(data) {
  return axios
    .post(`${BASE_URL}/Sale`, data)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}

function deleteProductAPI(id) {
  return axios
    .delete(`${BASE_URL}/Sale/${id}`)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}

// update truyền 2 tham số (id, data)
function updateProductAPI(id, data) {
  return axios
    .put(`${BASE_URL}/Sale/${id}`, data)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}
