function init() {
  loadProductFromAPI().then((data) => {
    renderListProduct(data);
    console.log("data: ", data);
  });
}

function renderListProduct(data) {
  let contentHTML = "";
  data.forEach(function (list) {
    let productObj = new Product(
      list.id,
      list.name,
      list.price,
      list.screen,
      list.backCamera,
      list.frontCamera,
      list.img,
      list.desc,
      list.type
    );
    const product = JSON.stringify(productObj);
    contentHTML += `<tr>
            <td>${productObj.id}</td>
            <td>${productObj.name}</td>
            <td>${productObj.price}</td>
            <td><img width="50px" src="${productObj.img}" alt="" />
            </td>
            <td>${productObj.desc}</td>
            <td>
                <button class="btn btn-danger" onclick="deleteProduct(${productObj.id})">Xóa</button>
                <button class="btn btn-info" onclick='openUpdateProduct(${product})'>Sửa</button>
            </td>
        </tr>`;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

// lưu và cập nhật dùng chung hàm (sử dụng type="hidden" dùng ẩn "id")
function saveProduct() {
  let data = getFormData();
  // idValid = true;
  console.log("data: ", data);
  if (data.id) {
    updateProductAPI(data.id, data).then(function (res) {
      // isValid = isValid & kiemTraRong(data.name, "namSpan");
      init();
    });
  } else {
    saveProductToAPI(data).then(function (res) {
      init();
    });
  }
  closeModal();
}

function deleteProduct(id) {
  console.log(id);
  deleteProductAPI(id).then(function (res) {
    init();
  });
}

function openUpdateProduct(data) {
  loadDataToForm(data);
}

function getFormData() {
  let id = document.getElementById("id").value.trim();
  let name = document.getElementById("name").value.trim();
  let price = document.getElementById("price").value.trim();
  let screen = document.getElementById("screen").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let img = document.getElementById("img").value.trim();
  let desc = document.getElementById("desc").value.trim();
  let type = document.getElementById("typePhone").value;
  console.log(type);

  // trả thông tin từ form về obj
  return new Product(
    id || undefined,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
}

function loadDataToForm(data) {
  $("#myModal").modal("show");
  document.getElementById("id").value = data.id;
  document.getElementById("name").value = data.name;
  document.getElementById("price").value = data.price;
  document.getElementById("screen").value = data.screen;
  document.getElementById("backCamera").value = data.backCamera;
  document.getElementById("frontCamera").value = data.frontCamera;
  document.getElementById("img").value = data.img;
  document.getElementById("desc").value = data.desc;
  document.getElementById("typePhone").value = data.type;
}

//  rest form // đóng form
function closeModal() {
  document.getElementById("id").value = "";
  document.getElementById("name").value = "";
  document.getElementById("price").value = "";
  document.getElementById("screen").value = "";
  document.getElementById("backCamera").value = "";
  document.getElementById("frontCamera").value = "";
  document.getElementById("img").value = "";
  document.getElementById("desc").value = "";
  document.getElementById("typePhone").value = "";
  $("#modal-body").modal("hide");
  $("#myModal").modal("hide");
}

// mở form
// $('#myModal').modal('show');
